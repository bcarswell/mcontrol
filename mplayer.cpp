//#include <sdbus-c++/sdbus-c++.h>
#include <gio/gio.h>
#include <thread>
#include <iostream>
#include <functional>


template <typename GLib_t>
struct gobject_deleter
{
  void operator()(GLib_t * object)
  {
    g_object_unref(object);
  }
};
template <typename GLib_t>
using glib_ptr = std::unique_ptr<GLib_t, gobject_deleter<GLib_t> >;

class dbus_adapter {
public:

  void Connect() {

    g_bus_get(
      G_BUS_TYPE_SESSION,
      nullptr,
      dbus_adapter::on_connect,
      this);

  }

private:

  glib_ptr<GDBusProxy> m_proxy;
  glib_ptr<GDBusConnection> m_connection_ptr;

  static void on_connect(GObject *, GAsyncResult *res, gpointer user_data)
  {
    dbus_adapter * dbus = static_cast<dbus_adapter *>(user_data);

    GError *error = nullptr;

    dbus->m_connection_ptr.reset(g_bus_get_finish(res, &error));

    if (!dbus->m_connection_ptr)
    {
      std::cerr << error->message;
    }
    else 
    {
      std::cerr << "Connected";
    }

    if (error)
      g_error_free(error);
  }
  GDBusConnection * m_connection;
};


int main(int argc, char *argv[])
{
  (void)argc;
  (void)argv;

  dbus_adapter dbus;

  dbus.Connect();
/*
    auto connection = sdbus::createSessionBusConnection();

    const std::string destinationName("org.mpris.MediaPlayer2.spotify");
    const std::string objectPath("/org/mpris/MediaPlayer2");

    auto playerProxy = sdbus::createObjectProxy(*connection, destinationName, objectPath);

    const std::string interface("org.mpris.MediaPlayer2.Player");

    playerProxy->callMethod("PlayPause").onInterface(interface);
    */

  GMainLoop *loop;
 
  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);

  return 0;
}